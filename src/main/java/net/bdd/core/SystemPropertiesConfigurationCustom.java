package net.bdd.core;

import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.webdriver.SupportedWebDriver;
import net.thucydides.core.webdriver.SystemPropertiesConfiguration;

/**
 * Created by ntngoc on 6/21/2015.
 */
public class SystemPropertiesConfigurationCustom extends SystemPropertiesConfiguration {

    public SystemPropertiesConfigurationCustom(EnvironmentVariables environmentVariables) {
        super(environmentVariables);
    }

    public SupportedWebDriver getDriverType() {
        SupportedWebDriver custom_driver = super.getDriverType();

        return custom_driver;
    }
}

package net.bdd.core;

/**
 * Created by ntngoc on 6/21/2015.
 */

import net.serenitybdd.jbehave.SerenityStories;
import net.thucydides.core.util.EnvironmentVariables;
import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.reporters.StoryReporterBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SerenityStoriesCustom extends SerenityStories {
    private static final Logger log = LoggerFactory.getLogger(SerenityStoriesCustom.class);

    public SerenityStoriesCustom() {
        EnvironmentVariables env = getSystemConfiguration().getEnvironmentVariables();
        //boolean useSingleSessionWindow = env.getPropertyAsBoolean("use.single.session.window", false);
//        log.info("\n\n------------------------------------------------------------------");
//        log.info(env.getProperty("ngoc.serenity.report", "none.html"));
//        log.info("" + env.getPropertyAsBoolean("ngoc.serenity.singlewindow", false));
//        log.debug("LOG DEBUG");
//        log.info("LOG INFO");
//        log.error("LOG ERROR");
//        log.info("DRIVER1:" + env.getProperty("webdriver.provided"));
//        log.info("DRIVER2:" + env.getProperty("webdriver.provided." + env.getProperty("webdriver.provided.type")));
//        log.info("------------------------------------------------------------------\n\n");
    }


    @Override
    public org.jbehave.core.configuration.Configuration configuration() {
        // TODO Auto-generated method stub
        Configuration config = super.configuration();
        StoryReporterBuilder reportBuilder = config.storyReporterBuilder();
//		reportBuilder.withFormats(Format.CONSOLE, Format.STATS);
//		config.useStoryReporterBuilder(reportBuilder);
        return config;
    }

}

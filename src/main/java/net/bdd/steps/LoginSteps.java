package net.bdd.steps;

import net.bdd.pages.LoginPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

import static org.assertj.core.api.Assertions.assertThat;

public class LoginSteps extends ScenarioSteps {

    LoginPage loginPage;

    @Step
    public void open_login_page() {
        loginPage.open();
    }

    @Step
    public void open_login_form() {
        loginPage.clickLoginBtnAndWaitShowLoginForm();
    }

    @Step
    public void fill_user_info_and_click_login(String userName, String passWord, String staySignedIn) {
        loginPage.fillUserInfoAndClickLogin(userName, passWord, staySignedIn);
    }

    @Step
    public void verify_login_success() {
        assertThat(loginPage.getTitle()).containsIgnoringCase("My Network");
    }

    @Step
    public void show_error_message() {
        assertThat(loginPage.errorMessage.getText().toString().contains("Username or password incorrect. Please try again."));
    }

}
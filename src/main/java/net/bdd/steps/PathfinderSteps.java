package net.bdd.steps;

import net.bdd.pages.LoginPage;
import net.bdd.pages.MenuPage;
import net.bdd.pages.PathfinderPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

import java.util.logging.Logger;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by ntngoc on 6/21/2015.
 */
public class PathfinderSteps extends ScenarioSteps {
    Logger log = Logger.getLogger("Test");
    PathfinderPage pathFinderPage;
    LoginPage loginPage;
    MenuPage menuPage;

    @Step
    public void login_system(String userName, String passWord, String staySignedIn) {
        loginPage.open();
        loginPage.clickLoginBtnAndWaitShowLoginForm();
        loginPage.fillUserInfoAndClickLogin(userName, passWord, staySignedIn);
    }

    @Step
    public void open_path_finder_page() {
        menuPage.openPathFinderPage();
    }

    @Step
    public void fill_path_finder_info(String nameSource, String nameTarget) {
        pathFinderPage.searchFor(nameSource, nameTarget);
    }

    @Step
    public void verify_search_result(int directRoutes, int indirectRoutes, int targetRoutes) {
        assertThat(pathFinderPage.getTitle()).containsIgnoringCase("Pathfinder");
        assertThat(pathFinderPage.directRoutes.getText()).containsIgnoringCase(String.valueOf(directRoutes));
        assertThat(pathFinderPage.indirectRoutes.getText()).containsIgnoringCase(String.valueOf(indirectRoutes));
        assertThat(pathFinderPage.targetRoutes.getText()).containsIgnoringCase(String.valueOf(targetRoutes));
    }

    @Step
    public void verify_error_validation() {
        assertThat(pathFinderPage.targetRoutes.getCssValue("background-color").contains("250,229,228"));
    }
}

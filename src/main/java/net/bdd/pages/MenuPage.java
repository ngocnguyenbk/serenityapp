package net.bdd.pages;

/**
 * Created by ntngoc on 6/21/2015.
 */

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;

public class MenuPage extends PageObject {
//    private static final org.slf4j.Logger log = LoggerFactory.getLogger(MenuPage.class);

    @FindBy(linkText = "Pathfinder")
    WebElementFacade pathFinderItem;

    public void openPathFinderPage() {
        pathFinderItem.waitUntilVisible().waitUntilEnabled().click();
        PathfinderPage pathfinderPage = this.switchToPage(PathfinderPage.class);
        pathfinderPage.open();
    }
}
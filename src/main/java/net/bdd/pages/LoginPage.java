package net.bdd.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@DefaultUrl("http://localhost:8080/axins")
public class LoginPage extends PageObject {
    private static final Logger log = LoggerFactory.getLogger(LoginPage.class);

    @FindBy(css = "a.ai-login-button")
    WebElementFacade loginBtn;

    @FindBy(css = "#loginForm")
    WebElementFacade loginFrm;

    @FindBy(name = "j_username")
    WebElementFacade username;

    @FindBy(name = "j_password")
    WebElementFacade password;

    @FindBy(id = "ai-login-button")
    WebElementFacade loginFrmBtn;

    @FindBy(className = "login-error")
    public
    WebElementFacade errorMessage;

    @FindBy(id = "rememberme")
    WebElementFacade rememberBtn;

    public void clickLoginBtnAndWaitShowLoginForm() {
        loginBtn.waitUntilEnabled();
        loginBtn.click();
        loginFrm.waitUntilVisible();
    }

    public void fillUserInfoAndClickLogin(String usernameTxt, String passwordTxt, String isRemember) {
        username.waitUntilVisible().waitUntilEnabled();
        password.waitUntilVisible().waitUntilEnabled();
        loginFrmBtn.waitUntilVisible().waitUntilEnabled();
        username.type(usernameTxt);
        password.type(passwordTxt);
        if (isRemember.contains("Y")) {
            rememberBtn.click();
        }
        loginFrmBtn.click();
    }

}
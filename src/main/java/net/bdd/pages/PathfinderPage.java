package net.bdd.pages;

/**
 * Created by ntngoc on 6/21/2015.
 */

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.Keys;

@DefaultUrl("http://localhost:8080/axins/paths?resource=people")
public class PathfinderPage extends PageObject {
    @FindBy(id = "source")
    WebElementFacade source;

    @FindBy(id = "target")
    public
    WebElementFacade target;

    @FindBy(css = "body ul.ui-autocomplete:eq(1)")
    WebElementFacade sourceAutocompletePopup;


    @FindBy(css = "body ul.ui-autocomplete:eq(2)")
    WebElementFacade targetAutocompletePopup;

    @FindBy(css = ".ai-paths-search.btn-orange")
    WebElementFacade getPathBtn;


    @FindBy(css = ".ai-direct-paths-container .ai-path-container-header > span")
    public WebElementFacade directRoutes;

    @FindBy(css = ".ai-indirect-paths-container")
    public WebElementFacade indirectRoutes;

    @FindBy(css = "ai-outside-paths-container .ai-path-container-header > span")
    public WebElementFacade targetRoutes;

    @FindBy(className = "login-error")
    WebElementFacade errorMessage;

    public void searchFor(String nameSource, String nameTarget) {
        if (!nameSource.isEmpty()) {
            source.waitUntilVisible().waitUntilEnabled().sendKeys(nameSource);
            source.sendKeys(Keys.ARROW_DOWN, Keys.ENTER);
            sourceAutocompletePopup.waitUntilNotVisible();
        }
        if (!nameTarget.isEmpty()) {
            target.waitUntilVisible().waitUntilEnabled().sendKeys(nameTarget);
            target.sendKeys(Keys.ARROW_DOWN, Keys.ENTER);
            targetAutocompletePopup.waitUntilNotVisible();
        }
        getPathBtn.waitUntilVisible().waitUntilEnabled().click();

//        directRoutes.waitUntilVisible();
//        indirectRoutes.waitUntilVisible();
    }
}
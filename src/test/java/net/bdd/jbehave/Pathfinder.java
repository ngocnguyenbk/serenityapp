package net.bdd.jbehave;

import net.bdd.steps.PathfinderSteps;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.BeforeScenario;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import java.util.logging.Logger;

/**
 * Created by ntngoc on 6/17/2015.
 */
public class Pathfinder {

    @Steps
    PathfinderSteps pathfinderSteps;

    @BeforeScenario
    public void setUpBeforeScenario() {
        pathfinderSteps.getDriver().manage().deleteAllCookies();
        pathfinderSteps.getDriver().manage().window().maximize();
    }

    @Given("I logged into the system")
    public void loginToSystem() {
        pathfinderSteps.login_system("ngoc.nguyen@axonactive.vn", "123456!A", "N");

    }

    @When("I search for routes from the source entity with $sourcePerson to the target entity with $targetPerson")
    public void searchForRoutesFromSourceToTarget(String sourcePerson, String targetPerson) {
        pathfinderSteps.open_path_finder_page();
        pathfinderSteps.fill_path_finder_info(sourcePerson, targetPerson);
    }

    @When("I search for routes without defining source and target entity")
    public void searchForRoutesWithoutSourceEntity() {
        pathfinderSteps.open_path_finder_page();
        pathfinderSteps.fill_path_finder_info("", "");
    }

    @Then("I found direct routes $directRoutes, indirect routes $indirectRoutes and target routes $targetRoutes")
    public void getErrorValidationTargetEntity(int directRoutes, int indirectRoutes, int targetRoutes) {
        pathfinderSteps.verify_search_result(directRoutes, indirectRoutes, targetRoutes);
    }

    @Then("I get the error validation at target entity")
    public void showErrorValidation() {
        Logger log = Logger.getLogger("");
        pathfinderSteps.verify_error_validation();
    }

    @When("I search for routes with an unknown target entity $sourcePerson")
    public void searchForRoutesWithAnUnknownTargetEntity(String sourcePerson) {
        pathfinderSteps.open_path_finder_page();
        pathfinderSteps.fill_path_finder_info(sourcePerson, "unknownTarget");
    }

    @When("I search for routes without defining target entity $sourcePerson")
    public void searchForRoutesWithoutDefiningTarget(String sourcePerson) {
        pathfinderSteps.open_path_finder_page();
        pathfinderSteps.fill_path_finder_info(sourcePerson, "");
    }
}

package net.bdd.jbehave;

import net.bdd.steps.LoginSteps;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.*;


public class Login {

    @Steps
    LoginSteps loginSteps;

    @BeforeScenario(uponType = ScenarioType.EXAMPLE)
    public void setUpBeforeScenario() {
        loginSteps.getDriver().manage().deleteAllCookies();
        loginSteps.getDriver().manage().window().maximize();
    }

    @AfterScenario(uponType = ScenarioType.EXAMPLE)
    public void finishScenario() {
        loginSteps.getDriver().close();
    }

    @Given("I am on the login page")
    public void goToPage() {
        loginSteps.open_login_page();
        loginSteps.open_login_form();
    }

    @When("I login with userName $userName and lastName $passWord $staySignedIn")
    public void loginWithUserNameAndPassWord(String userName, String passWord, String staySignedIn) {
        loginSteps.fill_user_info_and_click_login(userName, passWord, staySignedIn);
    }

    @Then("I login successfully")
    public void verifyLoginSuccessfully() {
        loginSteps.verify_login_success();
    }

    @Then("I get error message")
    public void showErrorMessage() {
        loginSteps.show_error_message();
    }

    @When("I close the web browser")
    public void closeWebBrowser() {
        loginSteps.getDriver().close();
    }

}

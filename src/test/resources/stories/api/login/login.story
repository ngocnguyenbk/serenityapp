Login Axon Insight
As an manager
I want to login to system

Scenario: Login to Axon Insight with correct account
Given I am on the login page
When I login with userName <userName> and lastName <passWord> <staySignedIn>
Then I login successfully

Examples:
|userName|passWord|staySignedIn|
|cuong.nguyennhat@axonactive.vn|123456!A|N|

Scenario: Login to Axon Insight with incorrect account
Given I am on the login page
When I login with userName <userName> and lastName <passWord> <staySignedIn>
Then I get error message

Examples:
|url|userName|passWord|
|http://localhost:8080/axins|||
|http://localhost:8080/axins/login|ngoc.nguyen@axonactive.vn||N|
|http://localhost:8080/axins/login||123456!A|false|

Scenario: User straight through logged in via 'Remember Me' token
Given I am on the login page
When I login with userName <userName> and lastName <passWord> <staySignedIn>
Then I login successfully
When I close the web browser
Given I am on the login page
Then I login successfully

Examples:
|url|userName|passWord|staySignedIn|
|http://localhost:8080/axins/login|ngoc.nguyen@axonactive.vn|123456!A|Y|






















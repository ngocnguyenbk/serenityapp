Find routes between two managers
As an manager
I want to find routes between two managers

Scenario: Find route with defining source and target entity
Given I logged into the system
When I search for routes from the source entity with <sourcePerson> to the target entity with <targetPerson>
Then I found direct routes <directRoutes>, indirect routes <indirectRoutes> and target routes <targetRoutes>
Examples:
|sourcePerson               |targetPerson           |directRoutes |indirectRoutes |targetRoutes
|Hans Roth (9000 st. Gallen)|Ruth Roth (Bern)       |0            |0              |39

Scenario: Find route without defining targer entity
Given I logged into the system
When I search for routes without defining source and target entity
Then I get the error validation at target entity

Scenario: Find route with an unknown target entity
Given I logged into the system
When I search for routes with an unknown target entity <sourcePerson>
Then I get the error validation at target entity
Examples:
|sourcePerson               |
|Hans Roth (1000 Lausanne)  |

Scenario: Find route without defining target entity
Given I logged into the system
When I search for routes without defining target entity <sourcePerson>
Then I get the error validation at target entity
Examples:
|sourcePerson               |
|Hans Roth (1000 Lausanne)  |